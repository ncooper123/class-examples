import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * ListTest.java example from the text.
 */
public class ListTest {

    public static void main(String[] args){
        //Setup list1.
        String[] colors = {"black", "yellow", "green", "blue", "violet", "silver"};
        List<String> list1 = new LinkedList<String>();
        for (String color : colors){
            list1.add(color);
        }

        //Setup list2.
        String[] colors2 = {"gold", "white", "brown", "blue", "gray", "silver"};
        List<String> list2 = new LinkedList<>();
        for (String color : colors2){
            list2.add(color);
        }

        list1.addAll(list2);
        printList(list1);

        convertToUppercaseString(list1);
        printList(list1);

        System.out.println("Deleting elements 4 to 6...");
        removeItems(list1, 4, 7);
        printList(list1);
        printReversedList(list1);
    }

    private static void printList(Iterable<String> list){
        System.out.println("list:");
        for (String color : list){
            System.out.printf("%s ",color);
        }
        System.out.println();
    }

    private static void convertToUppercaseString(List<String> list){
        ListIterator<String> iterator = list.listIterator();
        while (iterator.hasNext()){
            String color = iterator.next();
            iterator.set(color.toUpperCase());
        }
    }

    private static void removeItems(List<String> list, int start, int end){
        list.subList(start,end).clear();
    }

    private static void printReversedList(List<String> list){
        ListIterator<String> iterator = list.listIterator(list.size());
        System.out.println("Reversed List:");
        while (iterator.hasPrevious()){
            System.out.printf("%s ", iterator.previous());
        }
    }

}