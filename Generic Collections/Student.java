import java.util.*;
import java.util.ArrayList;

/**
 * A class modeling a student.
 */
public class Student implements Comparable<Student> {

    protected int age;
    protected String name;

    public Student(final String name, final int age){
        if (age < 0){
            throw new IllegalArgumentException("Age must be non-negative.");
        }
        else if (name == null || name.trim().length() == 0){
            throw new IllegalArgumentException("Name cannot be empty.");
        }
        this.name = name;
        this.age = age;
    }


    @Override
    public String toString(){
        return this.name + ", " + this.age;
    }

    @Override
    public boolean equals(final Object object){
        if (object instanceof Student){
            final Student other = (Student) object;
            return (this.age == other.age && other.name.equals(this.name));
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.age,this.name);
    }

    public static void main(String[] args){
        java.util.ArrayList<Student> list = new ArrayList<>();
        Student tony = new Student("Tony",8);
        list.add(new Student("Rebecca",2));
        list.add(tony);
        list.add(new Student("Maxwell",101));
        list.add(new Student("Frank",10));
        java.util.Collections.sort(list,new StudentComparator());
        System.out.println(list);
        int index = Collections.binarySearch(list,new Student("Nathan",28),new StudentComparator());
        System.out.println(index);

    }

    @Override
    public int compareTo(Student o) {
        return this.age - o.age;
    }
}
