import java.util.*;

/**
 * CollectionTest.java example from the text.
 */
public class CollectionTest {

    public static void main(String[] args){
        // Define an initial list.
        String[] colors = {"MAGENTA","RED","WHITE","BLUE","CYAN"};
        List<String> list = new java.util.ArrayList<>();
        for (String color : colors){
            list.add(color);
        }

        // Define a list of colors to remove.
        String[] removeColors = {"RED","WHITE","BLUE"};
        List<String> removeList = new java.util.ArrayList<String>();
        for (String color : removeColors){
            removeList.add(color);
        }

        // Print out the initial list.
        System.out.println("ArrayList: ");
        for (int count = 0; count < list.size(); count++){
            System.out.printf("%s ", list.get(count));
        }

        // Remove everything found in removeList from list.
        removeColors(list,removeList);
        System.out.printf("%n%nArrayList after calling removeColors:%n");
        for (String color : list){
            System.out.printf("%s ",color);
        }
    }

    /**
     * Removes any items from c1 that are found in c2.
     *
     * @param c1 The collection to remove from.
     * @param c2 The collection to check from.
     */
    private static void removeColors(Collection<String> c1, Collection<String> c2){
        Iterator<String> iterator = c1.iterator();
        while (iterator.hasNext()){
            String thisElement = iterator.next();
            if (c2.contains(thisElement)){
                iterator.remove();
            }
        }
    }

}