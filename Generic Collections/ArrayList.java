/**
 * A simple implementation of ArrayList, similar to java.util.ArrayList.
 */
public class ArrayList <E> {

    /** The number of items currently in the list. */
    private int size;

    /** Internal array storage. */
    private E[] data;

    /**
     *  Creates a new ArrayList with an initial capacity of 10.
     */
    public ArrayList(){
        this(10);
    }

    /**
     * Creates a new ArrayList with the specified capacity.
     * @param capacity The initial capacity available.
     */
    public ArrayList(final int capacity){
        if (capacity < 0){
            throw new IllegalArgumentException("Capacity must be non-negative.");
        }
        this.size = 0;
        this.data = (E[])new Object[capacity];
    }

    /**
     * Returns the number of elements in the list.
     *
     * @return The number of elements in the list.
     */
    public int getSize(){
        return this.size;
    }

    /**
     * Returns whether the ArrayList is empty.
     *
     * @return Whether this.getSize() == 0.
     */
    public boolean isEmpty(){
        return (this.size == 0);
    }

    /**
     * Returns the value at the specified index.
     *
     * @param index the index to return from, between 0 and this.getSize()-1.
     * @return The object at the specified index.s
     */
    public E get(final int index){
        if (index < 0 || index >= this.size){
            throw new IllegalArgumentException("Index out of list bounds.");
        }
        return this.data[index];
    }

    /**
     * Extends the size of the internal capacity, copying over all elements in the list.
     */
    private void grow(){
        final int newCapacity = (this.data.length == 0) ? 1 : (this.data.length * 2);
        final E[] newData = (E[])new Object[newCapacity];
        for (int i = 0; i < this.size; i++){
            newData[i] = this.data[i];
        }
        this.data = newData;
    }

    /**
     * Appends an object to the end of the list.
     *
     * @param object The object to add.
     */
    public void add(final E object){
        if (this.size == this.data.length){
            this.grow();
        }
        this.data[this.size] = object;
        this.size++;
    }

    /**
     * Sets a value at the specified index. Must either overwrite an existing index or append to the exact end
     * of the list (this.getSize()).
     *
     * @param index The index to set at, between 0 and this.getSize() (inclusive).
     * @param object The new value to set.
     */
    public void set(final int index, final E object){
        if (index < 0 || index > this.size){
            throw new IllegalArgumentException("Invalid set index: " + index + ".");
        }
        if (index == this.data.length){
            // We are adding to the end of the list.
            this.grow();
            this.size++;
        }
        this.data[index] = object;
    }

    /**
     * Inserts a value at the specified index, shifting all values after up one index. Index to add must either
     * be an already-populated index or the next index to be added at.
     *
     * @param index The index to insert, between 0 and this.getSize() (inclusive).
     * @param object The new value to insert.
     */
    public void insert(final int index, final E object){
        if (index < 0 || index > this.size){
            throw new IllegalArgumentException("Invalid insertion index: " + index + ".");
        }
        if (this.size+1 >= this.data.length){
            // Grow if necessary.
            this.grow();
        }
        for (int i = this.size+1; i > index; i--){
            // Copy everything past it to make room.
            this.data[i] = this.data[i-1];
        }
        this.size++;
        this.data[index] = object;
    }

    /**
     * Removes the object at the specified index.
     *
     * @param index The index to remove at, between 0 and this.getSize()-1.
     */
    public void remove(final int index){
        if (index < 0 || index >= this.size){
            throw new IllegalArgumentException("Index outside of list bounds.");
        }
        for (int i = index; i < this.size-1; i++){
            this.data[i] = this.data[i+1];
        }
        this.size--;
    }

    /**
     * Returns an iterator over this list.
     *
     * @return A new ArrayListIterator for this list.
     */
    public ArrayListIterator iterator(){
        return new ArrayListIterator(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        for (int i = 0; i < this.getSize()-1; i++){
            stringBuilder.append(this.get(i).toString());
            stringBuilder.append(",");
        }
        if (this.getSize() > 0){
            stringBuilder.append(this.get(this.getSize()-1).toString());
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object object){
        if (object instanceof ArrayList){
            final ArrayList other = (ArrayList) object;
            if (other.getSize() != this.getSize()){
                return false;
            }
            else {
                for (int i = 0; i < this.getSize(); i++){
                    if (!other.get(i).equals(this.get(i))){
                        return false;
                    }
                }
                return true;
            }
        }
        else {
            return false;
        }
    }

}
