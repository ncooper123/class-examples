/**
 * A class representing a playing card.
 */
public class Card {

    public enum Face { ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING }
    public enum Suit { CLUBS, DIAMONDS, HEARTS, SPADES }

    private final Face face;
    private final Suit suit;

    public Card(final Face face,  final Suit suit){
        this.face = face;
        this.suit = suit;
    }

    public Face getFace(){
        return this.face;
    }

    public Suit getSuit(){
        return this.suit;
    }

    @Override
    public String toString(){
        return this.face.toString().toLowerCase() + " of " + this.suit.toString().toLowerCase();
    }

}
