import java.util.*;
import java.util.ArrayList;

public class DeckOfCards {

    private List<Card> list;

    public DeckOfCards(){
        this.list = new ArrayList<Card>();
        for (Card.Suit suit : Card.Suit.values()){
            for (Card.Face face : Card.Face.values()){
                this.list.add(new Card(face,suit));
            }
        }
        Collections.shuffle(list);
    }

    public void printCards(){
        for (Card card : this.list){
            System.out.println(card);
        }
    }

    public static void main(String[] args){
        DeckOfCards cards = new DeckOfCards();
        cards.printCards();
    }

}
