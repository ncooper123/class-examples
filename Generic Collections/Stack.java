import java.lang.reflect.Array;
import java.util.NoSuchElementException;

/**
 * Our own implementation of a Stack.
 */
public class Stack {

    private ArrayList data;

    public Stack(){
        this.data = new ArrayList();
    }

    public void push(Object value){
        this.data.add(value);
    }

    public Object peek(){
        if (this.data.isEmpty()){
            throw new NoSuchElementException();
        }
        return this.data.get(this.data.getSize()-1);
    }

    public Object pop(){
        if (this.data.isEmpty()){
            throw new NoSuchElementException();
        }
        final Object popped = this.data.get(this.data.getSize()-1);
        this.data.remove(this.data.getSize()-1);
        return popped;
    }

    public static void main(String[] args){
        Stack myStack = new Stack();
        myStack.push("Hello");
        myStack.push("World");
        myStack.push("!");
        Object value = myStack.pop();
        System.out.println(value);
    }


}
