import java.util.NoSuchElementException;

/**
 * A simple iterator over our ArrayList class.
 */
public class ArrayListIterator {

    /** The ArrayList we are traversing. */
    private final ArrayList arrayList;

    /** The index of the next element to return with next(). */
    private int position;

    /**
     * Constructs a new ArrayListIterator.
     *
     * @param arrayList The List to iterate over.
     */
    public ArrayListIterator(final ArrayList arrayList){
        this.arrayList = arrayList;
        this.position = 0;
    }

    /**
     * Returns whether there are remaining elements to iterate over.
     *
     * @return Whether there are more elements.
     */
    public boolean hasNext(){
        return (this.position < this.arrayList.getSize());
    }

    /**
     * Advances the iterator, returning the next element.
     *
     * @return The next element.
     * @throws NoSuchElementException if we are at the end of the list.
     */
    public Object next(){
        if (!this.hasNext()){
            throw new NoSuchElementException();
        }
        final Object result = this.arrayList.get(position);
        this.position++;
        return result;
    }

}
