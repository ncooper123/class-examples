
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetTest {

    public static void main(String[] args){
        Set<Student> myClass = new HashSet<>();

        Student joe = new Student("Joe",20);
        Student frank = new Student("Frank",20);
        Student mary = new Student("Mary",20);
        Student joe2 = new Student("Joe",20);

        System.out.println(myClass.add(joe));
        System.out.println(myClass.add(frank));
        System.out.println(myClass.add(mary));
        System.out.println(myClass.add(joe2));

        System.out.println(myClass);
    }

}
