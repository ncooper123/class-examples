import java.util.NoSuchElementException;

/**
 * Our own implementation of a Queue.
 */
public class Queue {

    private ArrayList data;

    public Queue(){
        this.data = new ArrayList();
    }

    public void offer(final Object object){
        this.data.add(object);
    }

    public Object poll(){
        if (this.data.isEmpty()){
            throw new NoSuchElementException();
        }
        final Object polled = this.data.get(0);
        this.data.remove(0);
        return polled;
    }

    public Object peek(){
        if (this.data.isEmpty()){
            throw new NoSuchElementException();
        }
        return this.data.get(0);
    }

    public static void main(String[] args){
        Queue myQueue = new Queue();
        myQueue.offer("Hello");
        myQueue.offer("World");
        myQueue.offer("!");
        Object peeked = myQueue.peek();
        System.out.println(peeked);
        System.out.println(myQueue.poll());
    }
}
