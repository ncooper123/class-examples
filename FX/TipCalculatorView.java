import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import java.util.Observable;
import java.util.Observer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

/**
 * A basic FX application for our tip calculator.
 *
 * This is the VIEW in the MVC pattern. It is interested in displaying information
 * from the model and responding to its updates by changing its displayed value.s
 *
 * We also want to extend javafx.application.Application to make this a top-level FX application.
 */
public class TipCalculatorView extends Application implements Observer {

    private static final NumberFormat CURRENCY = NumberFormat.getCurrencyInstance();
    private static final NumberFormat PERCENT = NumberFormat.getPercentInstance();

    static {
        CURRENCY.setRoundingMode(RoundingMode.HALF_UP);
    }

    public static void main(String[] args){
        Application.launch(args);
    }

    private FlowPane flowPane;
    private Label tipLabel;
    private Slider tipPercentageSlider;
    private TextField amountTextField;
    private TextField tipTextField;
    private TextField totalTextField;
    private TipCalculatorModel tipCalculatorModel;

    /**
     * All top-level FX applications should override the start() method. This is the method that will
     * be automatically called when our application is setup by the runtime.
     *
     * @param stage The stage to render to (which will be created for us automatically by the runtime).
     */
    @Override
    public void start(final Stage stage){
        this.setupFlowPane();
        stage.setTitle("Tip Calculator");
        stage.setScene(new Scene(this.flowPane,250,300));
        this.setupControls();
        this.setupListeners();
        this.tipCalculatorModel = new TipCalculatorModel(BigDecimal.ZERO,new BigDecimal("0.15"));
        this.tipCalculatorModel.addObserver(this);
        this.update(this.tipCalculatorModel,null);
        stage.show();
    }

    /**
     * We need to designate a ROOT node for our scene graph. Let's make it a flowPane, which automatically
     * organizes its children in a flow-based layout based on the window size and children dimensions.
     */
    private void setupFlowPane(){
        this.flowPane = new FlowPane();
        this.flowPane.setAlignment(Pos.CENTER);
        this.flowPane.setHgap(10);
        this.flowPane.setVgap(10);
        this.flowPane.setPadding(new Insets(10,10,10,10));
    }

    /**
     * Add labels, TextField for input/results, and a tip calculator slider.
     */
    private void setupControls(){
        // Amount
        Label label = new Label("Amount On Bill:");
        this.flowPane.getChildren().add(label);
        this.amountTextField = new TextField("0.00");
        this.flowPane.getChildren().add(this.amountTextField);

        // Setup the slider.
        this.tipLabel = new Label();
        this.flowPane.getChildren().add(this.tipLabel);
        this.tipPercentageSlider = new Slider(0,50,15);
        this.flowPane.getChildren().add(this.tipPercentageSlider);

        // Tip.
        label = new Label("Calculated Tip:");
        this.flowPane.getChildren().add(label);
        this.tipTextField = new TextField();
        this.tipTextField.setEditable(false);
        this.flowPane.getChildren().add(this.tipTextField);

        // Total.
        label = new Label("Calculated Total:");
        this.flowPane.getChildren().add(label);
        this.totalTextField = new TextField();
        this.tipTextField.setEditable(false);
        this.flowPane.getChildren().add(this.totalTextField);
   }

    /**
     * Setup our change listeners for responding to changes in the text input and slider values.
     *
     * These will be the CONTROLLERS in the MVC pattern and directly change the model.
     */
    public void setupListeners(){
        // Make the text field respond to changes in amount.
        this.amountTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                final BigDecimal amount = new BigDecimal(newValue);
                TipCalculatorView.this.tipCalculatorModel.setAmount(amount);
            }
        });

        // Make the percentage slider respond to changes in tip.
        this.tipPercentageSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                final BigDecimal amount = BigDecimal.valueOf(newValue.intValue() / 100.0);
                TipCalculatorView.this.tipCalculatorModel.setTipPercentage(amount);
            }
        });
    }

    /**
     * The view is listening to changes in the model. This method will run when the model changes and notifies
     * us. We should respond by updating our labels and text.
     * @param o The observable that called us.
     * @param arg The argument passed to us (not used here).
     */
    @Override
    public void update(final Observable o, final Object arg) {
        if (o == this.tipCalculatorModel){
            final BigDecimal percentage = this.tipCalculatorModel.getTipPercentage();
            this.tipLabel.setText("Tip Percentage: " + PERCENT.format(percentage));
            this.tipTextField.setText(CURRENCY.format(this.tipCalculatorModel.getTip()));
            this.totalTextField.setText(CURRENCY.format(this.tipCalculatorModel.getTotal()));
        }
    }
}