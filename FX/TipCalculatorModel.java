import java.math.BigDecimal;
import java.util.Observable;

/**
 * Our TipCalculatorModel.
 *
 * We want this class to hold the state of our calculator. This is the MODEL in the MVC pattern. We will
 * make it Observable so that it can inform its Observers (our FX view) of any changes.
 */
public class TipCalculatorModel extends Observable {

    /** The amount on the bill. */
    private BigDecimal amount;

    /** The selected tip percentage. */
    private BigDecimal tipPercentage;

    /** The calculated tip. */
    private BigDecimal tip;

    /** The total amount to pay. */
    private BigDecimal total;

    /**
     * Constructs a new TipCalculator with the given amount and tip percentage.
     * @param amount The amount on the bill.
     * @param tipPercentage The selected tip percentage.
     */
    public TipCalculatorModel(final BigDecimal amount, final BigDecimal tipPercentage){
        this.amount = amount;
        this.tipPercentage = tipPercentage;
        refresh();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getTipPercentage() {
        return tipPercentage;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Changes the amount on the bill and refreshes the computed values.
     *
     * @param amount The new amount.
     */
    public void setAmount(final BigDecimal amount){
        if (amount.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalArgumentException("Amount must be non-negative.");
        }
        this.amount = amount;
        refresh();
    }

    /**
     * Changes the tip amount and refreshes the computed values.
     *
     * @param tipPercentage The new tip amount.
     */
    public void setTipPercentage(final BigDecimal tipPercentage){
        if (tipPercentage.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalArgumentException("Tip percentage must be non-negative.");
        }
        this.tipPercentage = tipPercentage;
        refresh();
    }

    /**
     * Refreshes the computed values. Set ourselves to changed and notifies our observers.
     */
    private void refresh(){
        this.tip = this.amount.multiply(this.tipPercentage);
        this.total = this.amount.add(tip);
        this.setChanged();
        this.notifyObservers();
    }

}
