/**
 * A base digit. For each operation, it will also
 * act upon its associate if necessary.
 */
public class Digit implements VirtualDigit {

    /** The current value of this Digit. @invariant (value <= 9 && value >= 0) */
    private int value;

    /** The VirtualDigit to this digit's left. */
    private final VirtualDigit associate;

    /**
     * Creates a new Digit with a given associate.
     *
     * @param associate The digit to this digit's left.
     */
    public Digit(VirtualDigit associate){
        this.associate = associate;
        this.value = 0;
    }

    /**
     * {@InheritDoc}
     */
    @Override
    public void increment(){
        this.value++;
        if (this.value == 10){
            // This digit overflowed.
            this.value = 0;
            this.associate.increment();
        }
    }

    /**
     * {@InheritDoc}
     */
    @Override
    public void reset(){
        this.value = 0;
        this.associate.reset();
    }

    /**
     * {@InheritDoc}
     */
    @Override
    public String toString(){
        return this.associate.toString() + Integer.toString(this.value);
    }

}