/**
 * An example of recursion. The Fibonacci sequence is defined as:
 *
 * f(0) = 0
 * f(1) = 1
 * f(n) = f(n-1) + f(n-2)
 *
 * so, for example:
 *
 * 0 1 1 2 3 5 8 13 ...
 *
 */
public class Fib {

    /**
     * Prints the arg[0]th Fibonacci number.
     */
    public static void main(String[] args){
        System.out.println(fib(5));
    }

    /**
     * Computes the nth Fibonacci number recursively.
     *
     * @param n The Fibonacci index we are trying to compute.
     */
    public static int fib(int n){
        if (n == 0){
            //First base case. f(0) = 0.
            return 0;
        }
        else if (n == 1){
            //Second base case. f(1) = 1.
            return 1;
        }
        else {
            //General case. f(n) = f(n-1) + f(n-2).
            return fib(n-1) + fib(n-2);
        }
    }

}