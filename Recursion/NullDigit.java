/**
 * A "null" implementation of VirtualDigit. Represents a left-most
 * digit of the Odometer that is hidden and does not represent a number.
 */
public class NullDigit implements VirtualDigit {
    /**
     * {@InheritDoc}
     */
    @Override
    public void increment(){
        throw new ArithmeticException("The odometer has overflowed.");
    }

    /**
     *  {@InheritDoc}
     */
    @Override
    public void reset(){
        //Nothing to do here. End any recursion that a Digit class started.
    }

    /**
     * {@InheritDoc}
     */
    @Override
    public String toString(){
        //This digit is not visible in our Odometer.
        return "";
    }
}