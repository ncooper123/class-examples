/**
 * An example of Object (structural) recursion. A base interface for
 * a digit representation in our Odometer.
 */
public interface VirtualDigit {

    /**
     * Increments this digit by a single value. If we overflow from 9 to 0,
     * this method also increments our associate, if present.
     */
    void increment();

    /**
     * Resets this digit to 0, instructing the associate to do so as well.
     */
    void reset();

}