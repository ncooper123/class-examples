/**
 * Our Odometer class. Here we utilize our Digit/NullDigit classes to
 * represent the state of the Odometer. Since the Digit class exhibits
 * structural recursion, it will automatically cascade to its associates.
 * Thus, we only have to have a single reference to our one's digit for
 * our Odometer.
 */
public class Odometer {

    /** A reference to the left-most virtual digit. */
    private final VirtualDigit onesDigit;

    /**
     * Creates a new Odometer with the specified number of
     * physical digits.
     *
     * @param digits The number of digits in the Odometer. Must be positive.
     */
    public Odometer(int digits){
        if (digits < 1){
            throw new IllegalArgumentException("Invalid number of digits.");
        }
        VirtualDigit d = new NullDigit();
        for (int i = 0; i < digits; i++){
            //Keep appending digits from left to right.
            d = new Digit(d);
        }
        //Save a reference to the last digit we made.
        this.onesDigit = d;
    }

    /**
     * Increments the value of the Odometer by 1.
     *
     * @throws ArithmeticException If the Odometer overflows.
     */
    public void increment(){
        this.onesDigit.increment();
    }

    /**
     * Resets the Odometer to 0.
     */
    public void reset(){
        this.onesDigit.reset();
    }

    /**
     * Returns a String representation of this Odometer.
     *
     * @return The String value of this Odometer.
     */
    public String toString(){
        return this.onesDigit.toString();
    }

}