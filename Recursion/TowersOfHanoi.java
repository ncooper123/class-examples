/**
 * An example of solving a recursive problem -- the Towers of Hanoi. See
 * https://en.wikipedia.org/wiki/Tower_of_Hanoi for a good description
 * of the rules and some analysis of how to solve it.
 */
public class TowersOfHanoi {

    /**
     * Recursively solves the Towers of Hanoi. Prints out the algorithm to
     * standard output.
     *
     * @param disks The total number of disks we are using.
     * @param source The index of the first peg, where all disks start.
     * @param dest The index of the last peg, where all disks end.
     * @param temp The index of our temporary storage peg.
     */
    public static void solveTowers(int disks, int source, int dest, int temp){

        if (disks == 1){
            //Base case. We've reduced the problem to one ring.
            System.out.println(source + " --> " + dest);
        }
        else {
            //General recursive case.
            //1. Solve the n-1 case to move all disks on top of the largest to the temp.
            solveTowers(disks-1, source, temp, dest);
            //Move that original source peg to the actual dest.
            System.out.println(source + " --> " + dest);
            //Move the n-1 pegs back from the temp to the destination.
            solveTowers(disks-1, temp, dest, source);
        }
    }

    /**
     * Solves a simple case of moving three disks from the first peg
     * to the third peg using the second peg as storage.
     */
    public static void main(String[] args){
        solveTowers(3, 1, 3, 2);
    }

}