/**
 * A demo for the Odometer class. Instantiates a new Odometer
 * and increments it 100 times.
 */
public class OdometerDemo {

    public static void main(String[] args){
        final Odometer o = new Odometer(5);
        System.out.println(o);
        for (int i = 0; i < 100; i++){
            o.increment();
            System.out.println(o);
        }
        o.reset();
        System.out.println(o);
    }

}