import java.util.Observable;
import java.util.Observer;

public class Countdown extends Observable {


    public int getTimeRemaining() {
        return timeRemaining;
    }

    private int timeRemaining;

    @Override
    public synchronized void addObserver(Observer o) {
        o.update(this,this.timeRemaining);
        super.addObserver(o);
    }

    public Countdown(){
        this.timeRemaining = 10;
    }

    public void step(){
        this.timeRemaining--;
        this.setChanged();
        this.notifyObservers();
    }

}
