import java.util.Observable;
import java.util.Observer;

/**
 * Created by nathan on 2017-04-24.
 */
public class Partygoer implements Observer {

    public String name;

    public Partygoer(final String name){
        this.name = name;
    }

    public void shout(int value){
        if (value < 0){
            throw new IllegalArgumentException("What's going on?");
        }
        if (value > 0){
            System.out.println(this.name + ": " + value + "!");
        }
        else {
            System.out.println("Happy new year!");
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Countdown countdown = (Countdown) o;
        //Integer timeRemaining = (Integer) arg;
        this.shout(countdown.getTimeRemaining());
    }
}
