/**
 * Created by nathan on 2017-04-24.
 */
public class NewYearsEve {

    public static void main(String[] args){

        final Countdown countdown = new Countdown();

        final Partygoer nathan = new Partygoer("Nathan");
        final Partygoer hank = new Partygoer("Hank");
        final Partygoer chair = new Partygoer("Chair");

        countdown.addObserver(nathan);
        countdown.addObserver(hank);
        countdown.addObserver(chair);


        for (int i = 0; i < 10; i++){
            countdown.step();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e){
                System.exit(1);
            }
        }

    }
}
