/**
 * A normal driving strategy.
 */
public class NormalDrivingStrategy implements DrivingStrategy {

    @Override
    public void drive() {
        System.out.println("Whoops, almost went a mile over the speed limit.");
    }
}
