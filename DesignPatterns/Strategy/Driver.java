/**
 * Our Driver class, modeling a Driver.
 *
 * We want our drivers to have different methods of driving. Normally we could create
 * three subclasses, NormalDriver, AggressiveDriver, and PassiveDriver using regular inheritance.
 * Driver would be an abstract method with a drive() method that the subclasses must override.
 *
 * However, there are certain advantages to "moving out" the method of driving to its own class, the
 * DrivingStrategy class. Then we can give an instance of DrivingStrategy to our drivers. This is
 * what the strategy pattern is about.
 */
public class Driver {

    /** The driver's name.*/
    private String name;

    /** The license number. */
    private int licenseNumber;

    /** A DrivingStrategy instance that will dictate how they drive. */
    private DrivingStrategy drivingStrategy;

    /** Constructs a new Driver with the given strategy and variables. */
    public Driver(final DrivingStrategy drivingStrategy, final String name, final int licenseNumber) {
        this.drivingStrategy = drivingStrategy;
        this.name = name;
        this.licenseNumber = licenseNumber;
    }

    /**
     * Drives. Since we are using a strategy pattern here, we can just delegate
     * the hard work to our drivingStrategy variable. This is completely optional,
     * as someone could just say: myDriver.getDrivingStrategy().drive() to accomplish
     * the same thing.
     */
    public void drive(){
        this.drivingStrategy.drive();
    }

    /** Returns this driver's strategy. */
    public DrivingStrategy getDrivingStrategy() {
        return drivingStrategy;
    }

    /**
     * Sets this driver's driving strategy. This is super important,
     * as we can now hot-swap the strategy during run time -- something
     * not possible with just a regular inheritance model.
     *
     * @param drivingStrategy The new strategy to use.
     */
    public void setDrivingStrategy(DrivingStrategy drivingStrategy){
        this.drivingStrategy = drivingStrategy;
    }

    /**
     * Demo. Setup some drivers and drive them around.
     */
    public static void main(String[] args){

        DriverFactory factory = new DriverFactory();
        Driver blandBlandington = factory.createNormalDriver("Bland",333);
        Driver ruthM = factory.createPassiveDriver("Ruth",444);
        Driver joeSpeedington = factory.createAggressiveDriver("Joe",4544);

        blandBlandington.drive();
        ruthM.drive();
        joeSpeedington.drive();

        // Ruth gets more aggressive. Update her strategy.
        ruthM.setDrivingStrategy(new AggressiveDrivingStrategy());
        ruthM.drive();
    }



}
