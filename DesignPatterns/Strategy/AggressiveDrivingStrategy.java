/**
 * An aggressive driving strategy.
 */
public class AggressiveDrivingStrategy implements DrivingStrategy {

    @Override
    public void drive() {
        System.out.println("Get out of the way, sir. I'm late.");
    }
}
