/**
 * Bonus design pattern: The Factory Pattern.
 *
 * It is a little cumbersome to have to instantiate every driver
 * using all three arguments in the constructor. So we can simplify
 * the constructor invocations by creating "shortcut" methods in
 * this "factory" class that create common types of Drivers.
 */
public class DriverFactory {

    /**
     * Creates a new Driver Factory.
     *
     * No instance variables to modify. In fact, we could probably just make
     * this constructor private and all of our methods static if we
     * wanted to.
     */
    public DriverFactory(){

    }

    /** Creates an aggressive driver. */
    public Driver createAggressiveDriver(String name, int licenseNumber){
        return new Driver(new AggressiveDrivingStrategy(),name,licenseNumber);
    }

    /** Creates a passive driver. */
    public Driver createPassiveDriver(String name, int licenseNumber){
        return new Driver(new PassiveDrivingStrategy(),name,licenseNumber);
    }

    /** Creates a normal driver. */
    public Driver createNormalDriver(String name, int licenseNumber){
        return new Driver(new NormalDrivingStrategy(),name,licenseNumber);
    }

}
