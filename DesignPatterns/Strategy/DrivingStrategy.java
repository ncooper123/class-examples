/**
 * Our abstraction of how to drive. This is the "strategy" part of the strategy pattern.
 *
 * We can give instances of our strategy to driver instances, thereby delegating their behavior to the
 * strategy. If we had used a traditional inheritance hierarchy to model different drivers, they would be
 * "locked in" to that behavior should we decide later that they need to drive a different way.
 */
public interface DrivingStrategy {

    /**
     * The drive method we need to implement.
     */
    void drive();

}
