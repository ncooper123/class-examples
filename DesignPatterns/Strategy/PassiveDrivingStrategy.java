/**
 * A passive driving strategy.
 */
public class PassiveDrivingStrategy implements DrivingStrategy {

    @Override
    public void drive() {
        System.out.println("Oh golly, you go first. I'm scared to leave first gear.");
    }
}
