import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An implementation of a singly-linked list.
 */
public class LinkedList<T> implements Iterable<T> {

    /** The number of elements currently in the list. */
    private int size;

    /** A reference to the first ListNode in the List, or null if the list is empty. */
    private ListNode<T> firstNode;

    /** A reference to the last ListNode in this list, or null if the list is empty. */
    private ListNode<T> lastNode;

    /** Constructs a new, empty LinkedList. */
    public LinkedList(){
        this.size = 0;
        this.firstNode = this.lastNode = null;
    }

    /**
     * Returns whether this LinkedList is empty.
     *
     * @return Whether this LinkedList is empty.
     */
    public boolean isEmpty(){
        return (this.size == 0);
    }

    /**
     * Returns the number of elements in this LinkedList.
     *
     * @return The current size of the list.
     */
    public int getSize(){
        return this.size;
    }

    public void insertAtFront(final T data){
        if (this.isEmpty()){
            this.firstNode = this.lastNode = new ListNode<>(data);
        }
        else {
            this.firstNode = new ListNode<>(data,this.firstNode);
        }
        this.size++;
    }

    public void insertAtBack(final T data){
        final ListNode<T> newListNode = new ListNode<>(data);
        if (this.isEmpty()){
            // The list is empty. Make the new node the only node.
            this.firstNode = this.lastNode = newListNode;
        }
        else {
            this.lastNode.setNextNode(newListNode);
            this.lastNode = this.lastNode.getNext();
        }
        this.size++;
    }

    public T removeFromFront() {
        if (this.isEmpty()){
            throw new NoSuchElementException();
        }
        T removedItem = firstNode.getData();
        if (this.firstNode == this.lastNode){
            this.firstNode = this.lastNode = null;
        }
        else {
            this.firstNode = this.firstNode.getNext();
        }
        this.size--;
        return removedItem;
    }

    public T removeFromBack() {
        if (this.isEmpty()){
            throw new NoSuchElementException();
        }
        T removedItem = lastNode.getData();
        if (this.firstNode == this.lastNode){
            this.firstNode = this.lastNode = null;
        }
        else {
            ListNode<T> current = this.firstNode;
            while (current.getNext() != this.lastNode){
                current = current.getNext();
            }
            this.lastNode = current;
            current.clearNextNode();
        }
        this.size--;
        return removedItem;
    }

    public T get(final int index){
        if (index < 0 || index >= this.size){
            throw new IllegalArgumentException("Invalid index given.");
        }
        int i = 0;
        ListNode<T> current = this.firstNode;
        while (i < index){
            current = current.getNext();
            i++;
        }
        return current.getData();
    }

    public T remove(final int index){
        if (index < 0 || index >= this.size){
            throw new IllegalArgumentException("Invalid index given.");
        }
        if (index == this.size-1){
            return this.removeFromBack();
        }
        else if (index == 0){
            return this.removeFromFront();
        }
        else {
            int i = 0;
            ListNode<T> current = this.firstNode;
            T removed = null;
            while (i < index) {
                i++;
                if (i == index){
                    removed = current.getNext().getData();
                    current.setNextNode(current.getNext().getNext());
                    this.size--;
                }
                else {
                    current = current.getNext();
                }
            }
            return removed;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        int i = 0;
        ListNode<T> current = this.firstNode;
        while (i < this.size) {
            stringBuilder.append(current.getData().toString());
            if (i < this.size-1){
                stringBuilder.append(",");
            }
            current = current.getNext();
            i++;
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object object){
        if (object instanceof LinkedList){
            final LinkedList<T> other = (LinkedList<T>) object;
            if (other.getSize() != this.getSize()){
                return false;
            }
            else {
                int i = 0;
                ListNode<T> currentLeft = this.firstNode, currentRight = other.firstNode;
                while (i < this.size) {
                    if (!currentLeft.equals(currentRight)){
                        return false;
                    }
                    currentLeft = currentLeft.getNext();
                    currentRight = currentRight.getNext();
                    i++;
                }
                return true;
            }
        }
        else {
            return false;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }

    /**
     * A class that holds a data of type V and a pointer to the next node.
     */
    private static class ListNode<V> {

        /** The data. */
        private final V data;

        /** A pointer to the next node. */
        private ListNode<V> nextNode;

        /**
         * Constructs a new ListNode with the given data and no nextNode.
         *
         * @param object The data to hold.
         */
        ListNode(V object){
            this(object,null);
        }

        /**
         * Constructs a new ListNode with the given data and nextNode.
         * @param data The data to hold.
         * @param nextNode The next object.
         */
        ListNode(V data, ListNode<V> nextNode){
            this.data = data;
            this.nextNode = nextNode;
        }

        /**
         * Returns the data value of this Node.
         *
         * @return This node's data value.
         */
        V getData(){
            return this.data;
        }

        /**
         * Returns the nextNode pointer.
         *
         * @return This node's next node.
         */
        ListNode<V> getNext(){
            return this.nextNode;
        }

        /**
         * Overwrites the nextNode of this ListNode.
         *
         * @param nextNode The new nextNode value.
         */
        void setNextNode(ListNode<V> nextNode){
            this.nextNode = nextNode;
        }

        /**
         * Sets the Node's nextNode to null.
         */
        void clearNextNode(){
            this.nextNode = null;
        }

    }

    public class LinkedListIterator implements Iterator<T> {

        private ListNode<T> element;

        private LinkedListIterator(){
            System.out.println("Size: " + LinkedList.this.getSize());
            this.element = LinkedList.this.firstNode;
        }

        @Override
        public boolean hasNext() {
            return this.element != null;
        }

        @Override
        public T next() {
            if (this.element == null){
                throw new NoSuchElementException();
            }
            ListNode<T> result = this.element;
            this.element = this.element.nextNode;
            return result.data;
        }
    }

    public static void main(String[] args){
        LinkedList<Integer> list = new LinkedList<>();
        list.insertAtBack(3);
        list.insertAtBack(4);
        list.insertAtBack(9);
        list.insertAtBack(10);
        for (Integer integer : list){
            System.out.println(integer);
        }
    }


}