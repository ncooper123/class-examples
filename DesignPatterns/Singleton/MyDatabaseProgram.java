/**
 * Created by nathan on 2017-04-19.
 */
public class MyDatabaseProgram {

    public static void main(String[] args){
        GiantDatabase database = GiantDatabase.getInstance();
        System.out.println("the answer to the universe is " + database.getData()[42]);
        System.out.println("number of students" + database.getData()[500]);
    }
}
