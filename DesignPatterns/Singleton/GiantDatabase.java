/**
 * A GiantDatabase. We want to limit instances of this class to where only one can be created.
 *
 * This is where the singleton pattern comes in...
 */
public class GiantDatabase {

    /**
     * Create a single STATIC instance of this class. This will be the only instance that ever exists.
     */
    private static GiantDatabase instance;

    /**
     * Make the constructor private. If we define a private constructor as the only constructor in the class,
     * outside code cannot ever create instances. We've blocked them from creating them...
     */
    private GiantDatabase(){
        this.data = new long[10000000];
        this.PI = Math.PI;
    }

    /**
     * Now, all we have to do is define some facility for outside code to get access to our static instance. We
     * do this through a static getInstance() method. If the instance isn't yet setup, create it. Then return it.
     *
     * @return The single, static instance of this class.
     */
    public static GiantDatabase getInstance(){
        if (instance == null){
            instance = new GiantDatabase();
        }
        return instance;
    }

    // The rest of the class. Regular instance variables, etc.

    public long[] getData() {
        return data;
    }

    public double getPI() {
        return PI;
    }

    private long[] data;
    public double PI;
}
