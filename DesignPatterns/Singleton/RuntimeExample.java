/**
 * Created by nathan on 2017-04-19.
 */
public class RuntimeExample {

    public static void main(String[] args){
        Runtime myRuntime = Runtime.getRuntime();
        System.out.println(myRuntime.availableProcessors());
    }
}
