/**
 * RegularPolygon implementation. This is some other class that we want to adapt into a proper shape. Assume
 * that we either can't or don't want to modify this source code.
 */
public class RegularPolygon {

    private String color;
    private int numberOfSides;
    private double sideLength;

    public RegularPolygon(String color, int numberOfSides, double sideLength){
        if (numberOfSides < 3){
            throw new IllegalArgumentException("Number of sides must be >= 3.");
        }
        if (sideLength <= 0){
            throw new IllegalArgumentException("Side length must be positive.");
        }
        this.color = color;
        this.numberOfSides = numberOfSides;
        this.sideLength = sideLength;
    }

    public String getName(){
        switch (this.numberOfSides){
            case 3:
                return "Triangle";
            case 4:
                return "Rectangle";
            case 5:
                return "Pentagon";
            case 6:
                return "Hexagon";
            case 7:
                return "Septagon";
            case 8:
                return "Octogon";
            case 9:
                return "Nonogon";
            case 10:
                return "Decagon";
            default:
                return "Regular " + this.numberOfSides + "-gon";
        }
    }

    public String toString(){
        return this.getName();
    }

    @Override
    public boolean equals(Object object){
        if (!(object instanceof RegularPolygon)){
            return false;
        }
        else{
            RegularPolygon other = (RegularPolygon) object;
            return this.numberOfSides == other.numberOfSides && this.sideLength == other.sideLength;
        }
    }

    public String getColor(){
        return this.color;
    }

    public int getNumberOfSides(){
        return this.numberOfSides;
    }

    public double getSideLength(){
        return this.sideLength;
    }

    public double perimeter(){
        return this.numberOfSides * this.sideLength;
    }

    public double area(){
        final double s = this.sideLength*this.sideLength*this.numberOfSides;
        final double b = 4.0 * Math.tan(Math.PI / this.numberOfSides);
        return s / b;
    }

}
