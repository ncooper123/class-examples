/**
 * An adapted RegularPolygon class. We want to fit the existing code (that somebody else wrote) in RegularPolygon and
 * have it fit our Shape interface that we created earlier. So we create this class, an adapter, that essentially
 * "wraps" an instance of RegularPolygon, our adaptee.
 */
public class RegularPolygonAdapter implements Shape {

    /** The instance we are wrapping (adapting). */
    private RegularPolygon adaptee;

    /**
     * Creates a new RegularPolygonAdapter with the given number of sides and side length.
     *
     * @param numberOfSides The number of sides in the Polygon.
     * @param sideLength The length of each side.
     */
    public RegularPolygonAdapter(final int numberOfSides, final double sideLength){
        // We see that the adaptee class has extra functionality we don't need, the color field. Don't worry about it.
        // Just set it to some dummy value (or maybe just leave it null). Nobody will be able to
        // access the color anyway since adaptee is private.
        this.adaptee = new RegularPolygon("yellow",numberOfSides,sideLength);
    }

    /** Our overridden Shape method. Delegate the hard work to the private instance. */
    @Override
    public double getArea() {
        return this.adaptee.area();
    }

    /** Again, use the delegate. */
    @Override
    public boolean equals(Object other){
        return this.adaptee.equals(other);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        return "I am a polygon of sides " + this.adaptee.getNumberOfSides() + " and side length " + this.adaptee.getSideLength();
    }

}
