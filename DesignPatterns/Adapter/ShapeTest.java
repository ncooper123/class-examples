public class ShapeTest {

    /**
     * Demonstration of using our adapted class. Create a bunch of shapes and call methods on them.
     */
    public static void main(String[] args){
        final Shape[] myShapes = new Shape[]{
            new Circle(4),
            new Rectangle(4,5),
            new Square(1),
            new RegularPolygonAdapter(8,2),
            new RegularPolygonAdapter(5,2.5),
        };
        for (int i = 0; i < myShapes.length; i++){
            System.out.println(myShapes[i].toString());
        }
    }
}
