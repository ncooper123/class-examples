import java.util.*;

/**
 * A class modeling a student.
 */
public class Student {

    protected int age;
    protected String name;

    public Student(final String name, final int age){
        if (age < 0){
            throw new IllegalArgumentException("Age must be non-negative.");
        }
        else if (name == null || name.trim().length() == 0){
            throw new IllegalArgumentException("Name cannot be empty.");
        }
        this.name = name;
        this.age = age;
    }


    @Override
    public String toString(){
        return this.name + ", " + this.age;
    }

    @Override
    public boolean equals(final Object object){
        if (object instanceof Student){
            final Student other = (Student) object;
            return (this.age == other.age && other.name.equals(this.name));
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.age,this.name);
    }
}
