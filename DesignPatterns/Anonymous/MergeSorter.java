import java.util.*;
public class MergeSorter implements Sorter
{
  /**
   * Sorts an array using MERGE SORT.
   *
   * ALGORITHM: Divide the list into two halves. Sort each half recursively,
   * then combine the sorted halves. This runs in O(n*lg(n)) time, roughly
   * because we have to divide the array until we encounter the base case,
   * an O(lg(n)) operation, each time performing the merge operation, an
   * O(n) operation.
   *
   * @param data The list to sort.
   * @author 	Anish Chand
   * @version 09 April 2017
   */

  private static int count = 0;
  private static int times = 0;
  public <E extends Comparable<E>> int sort(List<E> data)
  {
    mergeSort(data,0,data.size()-1);
    return count;
  }

  /**
   * Recursive helper method. Performs merge sort on a given sub-array between
   * the given start and end indices (inclusive).
   *
   * @param data The entire data array to sort.
   * @param start The first index of the section. Must be >= 0.
   * @param end The last index of the section. Must be >= start and < data.length.
   */
  private static<E extends Comparable<E>> void mergeSort(List <E> data, int start, int end){
      if (start >= end){
          /* Base case. We have a range consisting of only one element. There
          is nothing to do since a single element is already sorted. */
          return;
      }
      else {
          final int split = (start + end) / 2;
          mergeSort(data,start,split);    //Recursively sort the left-hand side.
          mergeSort(data,split+1,end);    //Recursively sort the right-hand side.
          merge(data,start,split+1,end);  //Now merge the two sorted sides.
      }
  }

  /**
   * Sorts an array that is already pre-sorted into two sorted halves.
   *
   * Works by creating a temporary array to house the elements within the
   * specified range. Uses the merge algorithm to fill this list by
   * examining the top elements on the left and right side until the copy is
   * full. Then overwrites the range in the original array with the new
   * copied data.
   *
   * @param data The entire data array to merge.
   * @param leftStart The first index of the left side.
   * @param rightStart The first index of the right side (>= leftStart).
   * @param rightEnd The last index of the right side (>= rightStart and < data.length).
   */
   private static <E extends Comparable<E>> void merge(List<E> data, int leftStart, int rightStart, int rightEnd){
      //Create a temporary copy of our data range.
      //E[] copy = (E[]) new Object[rightEnd-leftStart+1];
      List<E> copy = new ArrayList<>(rightEnd-leftStart+1);
      int k = 0;          //Current index into copy.

      int i = leftStart;  //Top of left.
      int j = rightStart; //Top of right.


      //Perform the merge while we have comparisons to make.
      while (i < rightStart && j <= rightEnd){
          /* Compare the leading values from the left and right side. Add the
          smaller value into the copy. */
        //  copy[k++] = (data.get(i).compareTo(data.get(j)) < 0) ? data.get(i++) : data.get(j++);
        if (data.get(i).compareTo(data.get(j)) < 0)
          {
            copy.add(k++, data.get(i++));
            count++;
          }
        else
          {
            copy.add(k++, data.get(j++));

          }
      }

      //We've exhausted the right side. Just bring in all elements from left.
      while (i < rightStart){
          copy.add(k++, data.get(i++));

      }

      //We've exhausted the left side. Just bring in all elements from right.
      while (j <= rightEnd){
          copy.add(k++, data.get(j++));

      }

      //Replace the original data with our copy
      int c = 0;
      int d = leftStart;
      //Setting sorted array to original one
      while(c< copy.size()){
            data.set(d, copy.get(c++));
            d++;
      }
  }


  public <E> int sort(List<E> data, Comparator<E> comparator)
  {
    mergeSort(data,0,data.size()-1, comparator);
    return times;
  }

  /**
   * Recursive helper method. Performs merge sort on a given sub-array between
   * the given start and end indices (inclusive).
   *
   * @param data The entire data array to sort.
   * @param start The first index of the section. Must be >= 0.
   * @param end The last index of the section. Must be >= start and < data.length.
   */
  private static<E> void mergeSort(List <E> data, int start, int end, Comparator<E> comparator)
  {
      if (start >= end){
          /* Base case. We have a range consisting of only one element. There
          is nothing to do since a single element is already sorted. */
          return;
      }
      else {
          final int split = (start + end) / 2;
          mergeSort(data,start,split, comparator);    //Recursively sort the left-hand side.
          mergeSort(data,split+1,end, comparator);    //Recursively sort the right-hand side.
          merge(data,start,split+1,end, comparator);  //Now merge the two sorted sides.
      }
  }

  /**
   * Sorts an array that is already pre-sorted into two sorted halves.
   *
   * Works by creating a temporary array to house the elements within the
   * specified range. Uses the merge algorithm to fill this list by
   * examining the top elements on the left and right side until the copy is
   * full. Then overwrites the range in the original array with the new
   * copied data.
   *
   * @param data The entire data array to merge.
   * @param leftStart The first index of the left side.
   * @param rightStart The first index of the right side (>= leftStart).
   * @param rightEnd The last index of the right side (>= rightStart and < data.length).
   */
   private static <E> void merge(List<E> data,int leftStart,int rightStart,int rightEnd,
                    Comparator<E> comparator)
    {
      //Create a temporary copy of our data range.
      //E[] copy = (E[]) new Object[rightEnd-leftStart+1];
      List<E> copy = new ArrayList<>();

      int k = 0;          //Current index into copy.

      int i = leftStart;  //Top of left.
      int j = rightStart; //Top of right.

      //Perform the merge while we have comparisons to make.
      while (i < rightStart && j <= rightEnd){
          /* Compare the leading values from the left and right side. Add the
          smaller value into the copy. */
        //  copy[k++] = (data.get(i).compareTo(data.get(j)) < 0) ? data.get(i++) : data.get(j++);
        if (comparator.compare(data.get(i),data.get(j)) < 0)
          {  copy.add(k++, data.get(i++));
            times++;
          }
        else
          {
            copy.add(k++, data.get(j++));
          }
      }

      //We've exhausted the right side. Just bring in all elements from left.
      while (i < rightStart){
          copy.add(k++, data.get(i++));
      }

      //We've exhausted the left side. Just bring in all elements from right.
      while (j <= rightEnd){
          copy.add(k++, data.get(j++));
      }

      //Replace the original data with our copy
      int c = 0;
      int d = leftStart;
      //Setting sorted array to original one
      while(c< copy.size()){
            data.set(d, copy.get(c++));
            d++;
      }
  }
}
