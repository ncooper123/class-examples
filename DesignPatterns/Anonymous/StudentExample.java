import java.util.Comparator;
import java.util.List;
import java.util.Vector;

/**
 * Created by nathan on 2017-04-26.
 */
public class StudentExample {

    public static void main(String[] args){

        List<Student> list = new Vector<>();
        list.add(new Student("Nathan",28));
        list.add(new Student("Hank",6));
        list.add(new Student("Chair",90));
        list.add(new Student("Bob",60));
        list.add(new Student("Table",11));

        MergeSorter myMergeSorter = new MergeSorter();
        myMergeSorter.sort(list,(a,b) -> a.age-b.age);

        for (Student student : list){
            System.out.println(student);
        }

    }

    public static Comparator<Student> getBoundsCheckingComparator(final int a, final int b){

        return new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                if (o1.age < a || o1.age > b || o2.age < a || o2.age > b){
                    throw new IllegalArgumentException("bad");
                }
                return o1.age - o2.age;
            }
        };

    }
}
