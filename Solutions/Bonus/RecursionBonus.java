/**
 * Recursion bonus problems.
 */
public class RecursionBonus {

    private RecursionBonus() {}

    /**
     * Returns whether the parameter is even.
     *
     * @param n The parameter to test.
     * @return True if n is even.
     */
    public static boolean isEven(final int n){
       if (n == 1){
           return false;
       }
       else if (n == 2){
           return true;
       }
       else {
           return isOdd(n-1);
       }
    }

    /**
     * Returns whether the parameter is odd.
     *
     * @param n The parameter to test.
     * @return True if n is odd.
     */
    public static boolean isOdd(final int n){
        if (n == 1){
            return true;
        }
        else if (n == 2){
            return false;
        }
        else {
            return isEven(n-1);
        }
    }

    /**
     * Returns the product of a and b.
     *
     * @param a The left operand.
     * @param b The right operand.
     * @return a * b.
     */
    public static int multiply(final int a, final int b){
        if (a == 0 || b == 0){
            // Base case. Anything times zero is zero.
            return 0;
        }
        else {
            // Recursive case. a * b = a + (a * b-1).
            return a + multiply(a,b-1);
        }
    }

    /**
     * Returns the number of times character c occurs in NCString string.
     *
     * @param string The String to search inside.
     * @param c The query character.
     * @return The number of times c occurs in string.
     */
    public static int characterCount(final NCString string, final char c){
        if (string.getLength() == 0){
            // Base case. The empty string can't contain c.
            return 0;
        }
        else if (string.getLength() == 1){
            // Base case 2. A 1-character string contains c 1 time if it equals c.
            return string.getFirstChar() == c ? 1 : 0;
        }
        else {
            // Recursive case. First detect if first or last characters are equal to c.
            int count = 0;
            count += (string.getFirstChar() == c ? 1 : 0);
            count += (string.getLastChar() == c ? 1 : 0);
            // Now add this sum to the character count of the middle characters.
            return count + characterCount(string.getMiddleChars(),c);
        }
    }

}
