/**
 * A class representing a special case of ComplexNumber, a RealNumber.
 *
 * @author Nathan Cooper
 */
public class RealNumber extends ComplexNumber {

    /**
     * Creates a new RealNumber with the specified real part.
     *
     * @param a The real part, a.
     */
    public RealNumber(final float a){
        super(a,0.0f);
    }

    /**
     * Returns whether this RealNumber is less than another.
     *
     * @param other The number to compare to.
     * @return Whether this number is less than the other.
     */
    public boolean isLessThan(final RealNumber other){
        return (this.a < other.a);
    }

    /**
     * Returns whether this RealNumber is greater than another.
     *
     * @param other The number to compare to.
     * @return Whether this number is greater than the other.
     */
    public boolean isGreaterThan(final RealNumber other){
        return (this.a > other.a);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        /*
         * OPTIONAL: We can Override toString again here to maybe speed up the computation of our toString value.
         * We know that the imaginary part is always zero here, so just return this.a as a String.
         */
        return String.valueOf(this.a);
    }

}
