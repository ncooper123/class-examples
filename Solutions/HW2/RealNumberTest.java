import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.*;

/**
 * A test class for RealNumber.
 *
 * @author Nathan Cooper
 */
public class RealNumberTest {

    public static final float EPSILON = ComplexNumberTest.EPSILON;

    private RealNumber zero, one, negativeOne, half, two;

    /**
     * Sets up our test fixtures.
     */
    @Before
    public void setupTestFixtures(){
        this.zero = new RealNumber(0.0f);
        this.one = new RealNumber(1.0f);
        this.negativeOne = new RealNumber(-1.0f);
        this.half = new RealNumber(0.5f);
        this.two = new RealNumber(2.0f);
    }

    /**
     * Tests that each fixture is initialized with the given real part and 0.0 for the imaginary part.
     */
    @Test
    public void testConstruction(){
        assertEquals(0.0f,zero.getA(),EPSILON);
        assertEquals(0.0f,zero.getB(),EPSILON);
        assertEquals(1.0f,one.getA(),EPSILON);
        assertEquals(0.0f,one.getB(),EPSILON);
        assertEquals(2.0f,two.getA(),EPSILON);
        assertEquals(0.0f,two.getB(),EPSILON);
        assertEquals(0.5f,half.getA(),EPSILON);
        assertEquals(0.0f,half.getB(),EPSILON);
    }

    /**
     * Verifies that isLessThan() works as intended.
     */
    @Test
    public void testIsLessThan(){
        assertTrue(zero.isLessThan(one));
        assertTrue(zero.isLessThan(half));
        assertTrue(zero.isLessThan(two));
        assertTrue(one.isLessThan(two));
        assertTrue(half.isLessThan(two));
        assertTrue(negativeOne.isLessThan(zero));
        assertTrue(negativeOne.isLessThan(two));
        assertTrue(negativeOne.isLessThan(one));
    }

    /**
     * Verifies that isGreaterThan() works as intended.
     */
    @Test
    public void testIsGreaterThan(){
        assertTrue(zero.isGreaterThan(negativeOne));
        assertTrue(one.isGreaterThan(zero));
        assertTrue(two.isGreaterThan(one));
        assertTrue(two.isGreaterThan(zero));
        assertTrue(two.isGreaterThan(negativeOne));
        assertTrue(half.isGreaterThan(zero));
        assertTrue(half.isGreaterThan(negativeOne));
    }


}
