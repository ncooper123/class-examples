/* Import all the static methods in the Assert class so we don't have to prefix every call with Assert. */
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

/**
 * A test class for ComplexNumber.
 *
 * @author Nathan Cooper
 */
public class ComplexNumberTest {

    /** A small number defining the precision we require for floating point equality. */
    public static final float EPSILON = 0.0000001f;

    private ComplexNumber zero, one, negativeOne, i, half, oneOne, negativeOneNegativeOne, two, twoI, nineNegativeEight;

    /**
     * Sets up test fixtures to be used in the @Test methods below.
     */
    @Before
    public void setupTestFixtures(){
        zero = new ComplexNumber(0.0f,0.0f);
        one = new ComplexNumber(1.0f,0.0f);
        negativeOne = new ComplexNumber(-1.0f,0.0f);
        i = new ComplexNumber(0.0f,1.0f);
        half = new ComplexNumber(0.5f,0.0f);
        oneOne = new ComplexNumber(1.0f,1.0f);
        negativeOneNegativeOne = new ComplexNumber(-1.0f,-1.0f);
        two = new ComplexNumber(2.0f,0.0f);
        twoI = new ComplexNumber(0.0f,2.0f);
        nineNegativeEight = new ComplexNumber(9.0f,-8.0f);
    }

    /**
     * Tests that the getA() method works as intended.
     */
    @Test
    public void testGetA(){
        assertEquals(0.0f,zero.getA(),EPSILON);
        assertEquals(1.0f,one.getA(),EPSILON);
        assertEquals(-1.0f,negativeOne.getA(),EPSILON);
        assertEquals(0.0f,i.getA(),EPSILON);
        assertEquals(0.5f,half.getA(),EPSILON);
        assertEquals(1.0f,oneOne.getA(),EPSILON);
        assertEquals(-1.0f,negativeOneNegativeOne.getA(),EPSILON);
        assertEquals(2.0f,two.getA(),EPSILON);
        assertEquals(0.0f,twoI.getA(),EPSILON);
        assertEquals(9.0f,nineNegativeEight.getA(),EPSILON);
    }

    /**
     * Tests that the getB() method works as intended.
     */
    @Test
    public void testGetB(){
        assertEquals(0.0f,zero.getB(),EPSILON);
        assertEquals(0.0f,one.getB(),EPSILON);
        assertEquals(0.0f,negativeOne.getB(),EPSILON);
        assertEquals(1.0f,i.getB(),EPSILON);
        assertEquals(0.0f,half.getB(),EPSILON);
        assertEquals(1.0f,oneOne.getB(),EPSILON);
        assertEquals(-1.0f,negativeOneNegativeOne.getB(),EPSILON);
        assertEquals(0.0f,two.getB(),EPSILON);
        assertEquals(2.0f,twoI.getB(),EPSILON);
        assertEquals(-8.0f,nineNegativeEight.getB(),EPSILON);
    }

    /**
     * Checks that the toString() method works as intended.
     */
    @Test
    public void testToString(){
        assertEquals("0.0",zero.toString());
        assertEquals("1.0",one.toString());
        assertEquals("-1.0",negativeOne.toString());
        assertEquals("0.5",half.toString());
        assertEquals("1.0i",i.toString());
        assertEquals("1.0+1.0i",oneOne.toString());
        assertEquals("-1.0-1.0i",negativeOneNegativeOne.toString());
    }

    /**
     * Checks that the equals() method works as intended.
     */
    @Test
    public void testEquals(){
        // Values should be equal to themselves.
        assertEquals(zero, zero);
        assertEquals(one, one);
        assertEquals(half, half);
        assertEquals(negativeOne, negativeOne);

        // Should be equal to logically equal others.
        assertEquals(new ComplexNumber(0.0f,0.0f), zero);
        assertEquals(new ComplexNumber(1.0f,0.0f), one);
        assertEquals(new ComplexNumber(-1.0f,-1.0f), negativeOneNegativeOne);

        // Should not be equal to obviously-unequal others.
        assertNotEquals(one, zero);
        assertNotEquals(negativeOne, zero);
        assertNotEquals(negativeOne, one);
        assertNotEquals(one, i);
    }

    /**
     * A helper method that asserts the equality of two ComplexNumbers for equality within EPSILON threshold.
     *
     * @param expected The right operand.
     * @param actual The left operand.
     */
    private static void assertComplexEquals(final ComplexNumber expected, final ComplexNumber actual){
        assertEquals(actual.getA(),expected.getA(),EPSILON);
        assertEquals(actual.getB(),expected.getB(),EPSILON);
    }

    /**
     * Checks that the add() method works as intended.
     */
    @Test
    public void testAdd(){
        // Adding zero shouldn't change anything.
        assertComplexEquals(zero, zero.add(zero));
        assertComplexEquals(one, zero.add(one));
        assertComplexEquals(negativeOne, zero.add(negativeOne));
        assertComplexEquals(i, zero.add(i));

        // Check other values.
        assertComplexEquals(new ComplexNumber(2.0f,0.0f), one.add(one));
        assertComplexEquals(new ComplexNumber(2.0f,2.0f), oneOne.add(oneOne));
        assertComplexEquals(new ComplexNumber(1.0f,1.0f), one.add(i));
        assertComplexEquals(new ComplexNumber(1.0f,1.0f), i.add(one));
        assertComplexEquals(new ComplexNumber(1.0f,1.0f), i.add(one));
        assertComplexEquals(zero, negativeOneNegativeOne.add(oneOne));
        assertComplexEquals(one, half.add(half));
    }

    /**
     * Checks that the subtract() method works as intended.
     */
    @Test
    public void testSubtract(){
        // Subtracting zero shouldn't change anything.
        assertComplexEquals(zero, zero.subtract(zero));
        assertComplexEquals(one, one.subtract(zero));
        assertComplexEquals(negativeOne, negativeOne.subtract(zero));
        assertComplexEquals(i, i.subtract(zero));

        // Check other values.
        assertComplexEquals(zero, one.subtract(one));
        assertComplexEquals(zero, oneOne.subtract(oneOne));
        assertComplexEquals(new ComplexNumber(1.0f,-1.0f), one.subtract(i));
        assertComplexEquals(new ComplexNumber(-1.0f,1.0f), i.subtract(one));
        assertComplexEquals(new ComplexNumber(-1.0f,1.0f), i.subtract(one));
        assertComplexEquals(new ComplexNumber(-2.0f,-2.0f), negativeOneNegativeOne.subtract(oneOne));
        assertComplexEquals(half, one.subtract(half));
    }

    /**
     * Checks that the multiply() method works as intended.
     */
    @Test
    public void testMultiply(){
        // Multiplying by zero should result in zero.
        assertComplexEquals(zero, zero.multiply(zero));
        assertComplexEquals(zero, zero.multiply(one));
        assertComplexEquals(zero, one.multiply(zero));
        assertComplexEquals(zero, oneOne.multiply(zero));
        assertComplexEquals(zero, i.multiply(zero));

        // Multiplying by one should result in itself.
        assertComplexEquals(oneOne, oneOne.multiply(one));
        assertComplexEquals(i, i.multiply(one));
        assertComplexEquals(negativeOneNegativeOne, negativeOneNegativeOne.multiply(one));

        // Other cases.
        assertComplexEquals(i, one.multiply(i));
        assertComplexEquals(new ComplexNumber(18.0f,-16.0f), two.multiply(nineNegativeEight));
        assertComplexEquals(new ComplexNumber(18.0f,-16.0f), nineNegativeEight.multiply(two));
        assertComplexEquals(new ComplexNumber(16.0f,18.0f), twoI.multiply(nineNegativeEight));
        assertComplexEquals(new ComplexNumber(16.0f,18.0f), nineNegativeEight.multiply(twoI));
        assertComplexEquals(new ComplexNumber(17.0f,-144.0f), nineNegativeEight.multiply(nineNegativeEight));
        assertComplexEquals(new ComplexNumber(4.5f,-4.0f), nineNegativeEight.multiply(half));
    }

    /**
     * Checks that the divide() method works as intended. Leave the special case of division by zero
     * for the method below.
     */
    @Test
    public void testDivide(){
        // Anything divided by one is itself.
        assertComplexEquals(one, one.divide(one));
        assertComplexEquals(nineNegativeEight, nineNegativeEight.divide(one));
        assertComplexEquals(twoI, twoI.divide(one));
        assertComplexEquals(oneOne, oneOne.divide(one));

        // Check some other cases.
        assertComplexEquals(new ComplexNumber(0.5f,0.0f), one.divide(two));
        assertComplexEquals(two, two.divide(one));
        assertComplexEquals(new ComplexNumber(4.0f,0.0f), two.divide(half));
        assertComplexEquals(new ComplexNumber(0.5f,-8.5f), nineNegativeEight.divide(oneOne));
        // Fractional forms below found from Wolfram Alpha.
        assertComplexEquals(new ComplexNumber(1.0f/145.0f,17.0f/145.0f), oneOne.divide(nineNegativeEight));
    }

    /**
     * Tests that divide() throws an ArithmeticException if the divisor is zero. Here, we can
     * catch the exception that we are expecting to get thrown in the illegal division. If the line
     * beneath it executes, something is wrong.
     */
    @Test
    public void testArithmeticException(){
        // One case.
        try {
            one.divide(zero);
            fail("Division by zero succeeded.");
        } catch (final ArithmeticException e){
            //Successful throw.
        } catch (final Exception e){
            fail("Improper type of exception thrown.");
        }

        // Try another case.
        try {
            negativeOneNegativeOne.divide(zero);
            fail("Division by zero succeeded.");
        } catch (final ArithmeticException e){
            //Successful throw.
        } catch (final Exception e){
            fail("Improper type of exception thrown.");
        }
    }

}
