/**
 * The main part of HW4. Implement both of these methods.
 */
public class StringMethods {

    /** No reason to instantiate this class, so the constructor is private. */
    private StringMethods(){ }

    ///////////////////////////////////////////////////////////
    // PART ONE
    ///////////////////////////////////////////////////////////

    /**
     * Returns whether two NCStrings are equal. The comparison respects case (so 'test' != 'Test').
     *
     * @return Whether two NCStrings are equal.
     */
    public static boolean areEqual(final NCString a, final NCString b){
        if (a.getLength() != b.getLength()){
            // Base case. The strings aren't the same length. They cannot be equal.
            return false;
        }
        else {
            // Otherwise, they are the same length...
            if (a.getLength() == 0){
                // Both are empty. They are equal.
                return true;
            }
            else if (a.getLength() == 1){
                // Base case 1. Both are length 1. Return whether the first (and only) characters are equal.
                return a.getFirstChar() == b.getFirstChar();
            }
            else {
                // Recursive case. Are equal if their first, last, and middle characters are equal.
                return (a.getFirstChar() == b.getFirstChar()) && (a.getLastChar() == b.getLastChar()) && areEqual(a.getMiddleChars(),b.getMiddleChars());
            }
        }
    }

    ///////////////////////////////////////////////////////////
    // PART TWO
    ///////////////////////////////////////////////////////////

    /**
     * Returns whether two NCStrings are palindromes. A String is a palindrome if it reads the
     * same forwards and backwards, respecting case and whitespace. E.g.:
     *
     * "racecar"
     * "level"
     * "a"
     * "aa"
     * "mom"
     * "radar radar"
     * ""           <-- the empty string.
     *
     * The following would not be considered palindromes:
     *
     * "example"
     * "top spot"   <-- because of the space
     * "Mom"        <-- because of the capitalization
     *
     * @param s The NCString to examine. Can be assumed to be non-null.
     * @return Whether the argument is a palindrome, as defined above.
     */
    public static boolean isPalindrome(final NCString s){
        if (s.getLength() <= 1){
            // Base case. String is empty or singleton.
            return true;
        }
        else {
            // Recursive case. True if first character is equal to the last AND the middle section is also a palindrome.
            return (s.getFirstChar() == s.getLastChar()) && isPalindrome(s.getMiddleChars());
        }
    }



}
