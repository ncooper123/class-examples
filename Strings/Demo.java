public class Demo {

    public static void main(String[] args){
        /*
         * There isn't very much exciting happening in Chapter 14. Just be
         * familiar with some of the String methods such as:
         *
         * length
         * charAt
         * indexOf
         * substring(int)
         * substring(int,int)
         *
         * String.valueOf(int)
         * String.valueOf(char)
         * .. etc.
         *
         * You will NOT be held responsible for memorizing anything related to regular expression syntax,
         * except maybe what they are in a general sense.
         */
    }


}
