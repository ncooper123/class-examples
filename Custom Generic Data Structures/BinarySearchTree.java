public class BinarySearchTree<T extends Comparable<T>> {

    private TreeNode<T> root;

    public BinarySearchTree() {
        this.root = null;
    }

    public void add(T data) {
        if (this.root == null) {
            this.root = new TreeNode<T>(data);
        } else {
            this.root.insert(data);
        }
    }

    public boolean contains(T data) {
        if (this.root == null) {
            return false;
        } else {
            return this.root.contains(data);
        }
    }

    public void traversePreorder() {
        if (this.root != null) {
            this.root.traversePreorder();
        }
    }

    public void traversePostorder() {
        if (this.root != null) {
            this.root.traversePostorder();
        }
    }

    public void traverseInOrder() {
        if (this.root != null) {
            this.root.traverseInOrder();
        }
    }

    public static void main(String[] args) {
        BinarySearchTree<Integer> binarySearchTree = new BinarySearchTree<>();
        binarySearchTree.add(5);
        binarySearchTree.add(8);
        binarySearchTree.add(17);
        binarySearchTree.add(13);
        binarySearchTree.add(0);
        binarySearchTree.add(-1);
        binarySearchTree.add(27);
        binarySearchTree.traversePostorder();
        //System.out.println(binarySearchTree.contains(8));
    }

}