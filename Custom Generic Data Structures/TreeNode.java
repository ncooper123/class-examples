class TreeNode<T extends Comparable<T>> {

    TreeNode<T> leftNode, rightNode;
    T data;

    public TreeNode(final T data){
        this.data = data;
        this.leftNode = this.rightNode = null;
    }

    public void insert(final T data){
        if (data.compareTo(this.data) < 0){
            //On left side of the tree.
            if (this.leftNode == null){
                this.leftNode = new TreeNode<T>(data);
            }
            else {
                this.leftNode.insert(data);
            }
        }
        else if (data.compareTo(this.data) > 0) {
            if (this.rightNode == null){
                this.rightNode = new TreeNode<T>(data);
            }
            else {
                this.rightNode.insert(data);
            }
        }
    }

    public boolean contains(final T data){
        int compare = data.compareTo(this.data);
        if (compare == 0){
            return true;
        }
        else if (compare < 0){
            if (this.leftNode == null){
                return false;
            }
            else {
                return this.leftNode.contains(data);
            }
        }
        else {
            if (this.rightNode == null){
                return false;
            }
            else {
                return this.rightNode.contains(data);
            }
        }
    }

    public void traversePreorder(){
        System.out.println(this.data);
        if (this.leftNode != null){
            this.leftNode.traversePreorder();
        }
        if (this.rightNode != null){
            this.rightNode.traversePreorder();
        }
    }

    public void traversePostorder(){
        if (this.leftNode != null){
            this.leftNode.traversePostorder();
        }
        if (this.rightNode != null){
            this.rightNode.traversePostorder();
        }
        System.out.println(this.data);
    }

    public void traverseInOrder(){
        if (this.leftNode != null){
            this.leftNode.traverseInOrder();
        }
        System.out.println(this.data);
        if (this.rightNode != null){
            this.rightNode.traverseInOrder();
        }
    }

}