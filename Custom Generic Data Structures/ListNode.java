/**
 * A class that holds a data of type T and a pointer to the next node.
 */
class ListNode<T> {

    /** The data. */
    private final T data;

    /** A pointer to the next node. */
    private ListNode<T> nextNode;

    /**
     * Constructs a new ListNode with the given data and no nextNode.
     *
     * @param object The data to hold.
     */
    ListNode(T object){
        this(object,null);
    }


    /**
     * Constructs a new ListNode with the given data and nextNode.
     * @param data The data to hold.
     * @param nextNode The next object.
     */
    ListNode(T data, ListNode<T> nextNode){
        this.data = data;
        this.nextNode = nextNode;
    }

    /**
     * Returns the data value of this Node.
     *
     * @return This node's data value.
     */
    T getData(){
        return this.data;
    }

    /**
     * Returns the nextNode pointer.
     *
     * @return This node's next node.
     */
    ListNode<T> getNext(){
        return this.nextNode;
    }

    /**
     * Overwrites the nextNode of this ListNode.
     *
     * @param nextNode The new nextNode value.
     */
    void setNextNode(ListNode<T> nextNode){
        this.nextNode = nextNode;
    }

    /**
     * Sets the Node's nextNode to null.
     */
    void clearNextNode(){
        this.nextNode = null;
    }

}