/**
 * Example of two different Searching algorithms.
 */
public class Searching {

    /**
     * Searches a given array for a searchKey using LINEAR SEARCH,
     * returning the first index where the key appears, or -1 if the searchKey
     * is not present. This runs in O(n) time.
     *
     * @param data The data array to search.
     * @param searchKey The integer key to search for.
     * @return The index of the searchKey, or -1 if not found.
     */
    public static int linearSearch(final Object[] data, final Object searchKey){
        //Scan from beginning to end, looking for our key.
        for (int i = 0; i < data.length; i++){
            if (data[i].equals(searchKey)){
                //We found the key. Return.
                return i;
            }
        }
        return -1; //The loop finished without returning a value.
    }

    /**
     * Searches a given array for a searchKey using BINARY SEARCH,
     * returning an index where the key appears, or -1 if the searchKey is not
     * present. Requires that the input array is in ascending sorted order.
     * This runs in O(lg(n)) time.
     *
     * PRECONDITION: THe data must be in ascending order.
     *
     * @param data The data array to search. Must be in sorted order.
     * @param searchKey The integer key to search for.
     * @return The index of the searchKey, or -1 if not found.
     */
    public static int binarySearch(final int[] data, final int searchKey){
        int low = 0;                        //Smallest possible index.
        int high = data.length - 1;         //Largest possible index.
        int index = (low + high + 1) / 2;   //Current search index.
        while (low <= high){
            if (searchKey == data[index]){
                //We found the searchKey at the current index. Just return it.
                return index;
            }
            else if (searchKey < data[index]){
                //The search value is smaller than the current value.
                //It must appear to the left side.
                System.out.println("Less than");
                high = index - 1;
            }
            else {
                //The search value is larger than the current value.
                //It must appear to the right side.
                low = index + 1;
            }
            index = (low + high + 1) / 2;
        }
        return -1; //The loop finished without completing a value.
    }

    //Test it out here...
    public static void main(String[] args){
        int[] data = new int[]{7,8,9,-1,3,4,5,6,7};
        //System.out.println(linearSearch(data,3));
        System.out.println(binarySearch(data,9));
    }

}