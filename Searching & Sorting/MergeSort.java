import java.util.Collections;

/**
 * Example of Merge Sort, an efficient sorting algorithm implemented using
 * recursion. While this is harder to implement than selection/insertion sort,
 * it can be much faster for larger n values.
 */
public class MergeSort {


    /**
     * Sorts an array using MERGE SORT.
     *
     * ALGORITHM: Divide the list into two halves. Sort each half recursively,
     * then combine the sorted halves. This runs in O(n*lg(n)) time, roughly
     * because we have to divide the array until we encounter the base case,
     * an O(lg(n)) operation, each time performing the merge operation, an
     * O(n) operation.
     *
     * @param data The data array to sort.
     */
    public static void mergeSort(int[] data){
        //Call our recursive helper on the entire array.
        mergeSort(data,0,data.length-1);
    }

    /**
     * Recursive helper method. Performs merge sort on a given sub-array between
     * the given start and end indices (inclusive).
     *
     * @param data The entire data array to sort.
     * @param start The first index of the section. Must be >= 0.
     * @param end The last index of the section. Must be >= start and < data.length.
     */
    private static void mergeSort(int[] data, int start, int end){
        if (start >= end){
            /* Base case. We have a range consisting of only one element. There
            is nothing to do since a single element is already sorted. */
            return;
        }
        else {
            final int split = (start + end) / 2;
            mergeSort(data,start,split);    //Recursively sort the left-hand side.
            mergeSort(data,split+1,end);    //Recursively sort the right-hand side.
            merge(data,start,split+1,end);  //Now merge the two sorted sides.
        }
    }

    /**
     * Sorts an array that is already pre-sorted into two sorted halves.
     *
     * Works by creating a temporary array to house the elements within the
     * specified range. Uses the merge algorithm to fill this list by
     * examining the top elements on the left and right side until the copy is
     * full. Then overwrites the range in the original array with the new
     * copied data.
     *
     * @param data The entire data array to merge.
     * @param leftStart The first index of the left side.
     * @param rightStart The first index of the right side (>= leftStart).
     * @param rightEnd The last index of the right side (>= rightStart and < data.length).
     */
    private static void merge(int[] data, int leftStart, int rightStart, int rightEnd){
        //Create a temporary copy of our data range.
        int copy[] = new int[rightEnd-leftStart+1];
        int k = 0;          //Current index into copy.

        int i = leftStart;  //Top of left.
        int j = rightStart; //Top of right.

        //Perform the merge while we have comparisons to make.
        while (i < rightStart && j <= rightEnd){
            /* Compare the leading values from the left and right side. Add the
            smaller value into the copy. */
            copy[k++] = (data[i] < data[j]) ? data[i++] : data[j++];
        }

        //We've exhausted the right side. Just bring in all elements from left.
        while (i < rightStart){
            copy[k++] = data[i++];
        }

        //We've exhausted the left side. Just bring in all elements from right.
        while (j <= rightEnd){
            copy[k++] = data[j++];
        }

        //Replace the original data with our copy using the built-in copy method.
        System.arraycopy(copy,0,data,leftStart,copy.length);
    }

    //Test it out here...
    public static void main(String[] args){
        int data[] = new int[]{19, -20, 1, 4, 9, -122, 100, 5, 6, 4, 2, 1, 3, 5, -12, 13};
        mergeSort(data);
        System.out.println(java.util.Arrays.toString(data));
    }

}