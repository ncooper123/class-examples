/**
 * Example of different sorting algorithms.
 */
public class Sorting {
    /**
     * A helper method to swap two values of an array.
     *
     * @param data The data array we want to manipulate.
     * @param i The first index. Must be a valid index of data[].
     * @param j The second index. Must be a valid index of data[].
     */
    private static void swap(final int[] data, final int i, final int j){
        final int t = data[i]; //Save a temporary value.
        data[i] = data[j];
        data[j] = t;
    }

    /**
     * Sorts an array using SELECTION SORT.
     *
     * ALGORITHM: Advance from the beginning to the end of the list, finding
     * the appropriate element that belongs at each position by searching
     * forward in the array. This runs in O(n^2) time.
     *
     * @param data The data array to sort.
     */
    public static void selectionSort(final int[] data){
        //For each index i in the data array...
        for (int i = 0; i < data.length - 1; i++){

            //Find the smallest element past i that is less than data[i].
            int indexOfSmallest = i;
            for (int j = i+1; j < data.length; j++){
                if (data[j] < data[indexOfSmallest]){
                    indexOfSmallest = j;
                }
            }

            //Swap the smallest value we found with the current value.
            swap(data,i,indexOfSmallest);
        }
    }

    /**
     * Sorts an array using INSERTION SORT.
     *
     * ALGORITHM: Advance from the beginning to the end of the list. For each
     * new item, insert it in the correct spot with respect to the part of the
     * list we've encountered already. This runs in O(n^2) time.
     *
     * @param data The data array to sort.
     */
    public static void insertionSort(final int[] data){

        // For each index i of the data array...
        for (int i = 0; i < data.length; i++){
            int currentElement = data[i];
            // Move each of our previously-sorted elements ahead one
            // if it's larger than the current.
            int j = i;
            while (j > 0 && data[j-1] > currentElement){
                data[j] = data[j-1]; //Shift element to the right one.
                j--;
            }
            //Now data[j] should be a free spot we can put currentElement.
            data[j] = currentElement;
        }
    }

    //Test it out here...
    public static void main(String[] args){
        int data1[] = new int[]{19, 1, 4, 9, 4, 2, 1, 3, 5, -12};
        selectionSort(data1);
        System.out.println(java.util.Arrays.toString(data1));

        int data2[] = new int[]{19, 1, 4, 9, 4, 2, 1, 3, 5, -12};
        insertionSort(data2);
        System.out.println(java.util.Arrays.toString(data2));
    }
}