import java.io.Serializable;

/**
 * A class modeling an Account.
 */
public class Account {

    /** The account number. */
    private int account;

    /** The account's first name. */
    private String firstName;

    /** The account's last name. */
    private String lastName;

    /** The account balance. */
    private double balance;

    /**
     * Helper constructor for Account. Creates an empty account with no number
     * of balance and empty strings as its name.
     */
    public Account(){
        this(0,"","",0.0); //Call the constructor below.
    }

    /**
     * Creates a new Account with the given parameters.
     *
     * @param account The account number.
     * @param firstName The account's first name.
     * @param lastName The account's last name.
     * @param balance The account's balance.
     */
    public Account(int account, String firstName, String lastName, double balance){
        this.account = account;
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
    }

    //GETTER METHODS

    public int getAccount(){
        return this.account;
    }

    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName(){
        return this.lastName;
    }

    public double getBalance(){
        return this.balance;
    }

}