import java.nio.file.*;
import java.util.Scanner;
import java.io.IOException;

public class FileAndDirectoryInfo {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a file or directory name: ");
        String s = scanner.nextLine();
        Path path = Paths.get(s);
        if (Files.exists(path)){
            System.out.println("File exists: " + path.getFileName());
            System.out.printf("%s a directory%n", Files.isDirectory(path) ? "Is" : "Is not");
            System.out.println(path.isAbsolute() ? "Is Absolute" : "Is Not Absolute");
            System.out.println(Files.size(path));
            System.out.println("Path: " + path.toString());
            System.out.println("Absolute Path: " + path.toAbsolutePath());
            if (Files.isDirectory(path)){
                DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);
                for (Path p : directoryStream){
                    System.out.println(p);
                }
            }

        }
        else {
            System.out.println("File does not exist.");
        }
    }

}