/**
 * An enumeration of potential menu options for the CreditInquiry program.
 */
public enum MenuOption {

    /** List accounts with zero balance. */
    ZERO_BALANCE,

    /** List accounts with a credit (negative) balance. */
    CREDIT_BALANCE,

    /** List accounts with a debit (positive) balance. */
    DEBIT_BALANCE,

    /** End the program. */
    END;
}