import java.lang.ArithmeticException;
import java.math.BigInteger;
import java.util.Objects;

/**
 * @author 	Franklin D. Worrell
 * @version 23 January 2016
 *
 * Modified by Nathan Cooper April 2017 with the following changes:
 *
 * 	- State is now final.
 * 	- Option to normalize the fraction in the denominator.
 * 	- Negative values are now forced to be in the numerator.
 */ 
public class Fraction {

	public static final Fraction ZERO = new Fraction(0,1,false);
	public static final Fraction ONE = new Fraction(1,1,false);

	private final int numerator;
	private final int denominator;

	public Fraction(final int numerator, final int denominator){
		this(numerator,denominator,false);
	}
	
	/**
	 * Initializes <code>numerator</code> and 
	 * <code>denominator</code> to values passed 
	 * as arguments. 
	 * 
	 * @param	numerator	the new Fraction's numerator
	 * @param	denominator	the new Fraction's denominator
	 */ 
	public Fraction(int numerator, int denominator, final boolean normalize) {
		if (denominator == 0){
			throw new IllegalArgumentException("Denominator cannot be zero.");
		}
		if (!normalize){
			this.numerator = numerator;
			this.denominator = denominator;
		}
		else if (numerator == 0){
			this.numerator = 0;
			this.denominator = 1;
		}
		else {
			// Normalize the fraction.
			final int gcd = gcd(numerator,denominator);
			int p = numerator;
			int q = denominator;
			if (gcd > 1){
				// Divide by a common factor if necessary.
				p /= gcd;
				q /= gcd;
			}
			if (denominator < 0){
				// Normalize so negative sign is always in the numerator.
				p = -p;
				q = -q;
			}
			this.numerator = p;
			this.denominator = q;
		}

	} // end constructor
	
	
	/** 
	 * Returns the <code>numerator</code> of 
	 * the <code>Fraction</code>. 
	 *
	 * @return	this.numerator
	 */ 
	public int getNumerator() {
		return this.numerator; 
	} // end method getNumerator
	
	
	/**
	 * 
	 */ 
	public int getDenominator() {
		return this.denominator; 
	} // end method getDenominator
	

	public Fraction add(final Fraction other) {
		final int common = this.denominator * other.denominator;
		final int p = (this.numerator * other.denominator) + (other.numerator * this.denominator);
		return new Fraction(p,common,true);
	} // end method add
	

	public Fraction subtract(Fraction other) {
		final int common = this.denominator * other.denominator;
		final int p = (this.numerator * other.denominator) - (other.numerator * this.denominator);
		return new Fraction(p,common,true);
	} // end method subtract 
	

	public Fraction multiply(Fraction other) {
		final int p = this.numerator * other.numerator;
		final int q = this.denominator * other.denominator;
		return new Fraction(p,q,true);
	} // end method multiply 
	

	public Fraction divide(Fraction other) throws ArithmeticException {
		// Attempted division by zero. 
		if (other.numerator == 0) {
			throw new ArithmeticException("Division by zero is undefined."); 
		}
		else {
			final int p = this.numerator * other.denominator;
			final int q = this.denominator * other.numerator;
			return new Fraction(p,q,true);
		} 
	} // end method divide
	
	
	/**
	 * Returns a String representation of the 
	 * <code>Fraction</code>. 
	 * 
	 * @return	a String representation of this
	 */ 
	@Override
	public String toString() {
		return this.numerator + " / " + this.denominator; 
	} // end method toString
	
	
	/**
	 * 
	 */ 
	@Override
	public boolean equals(Object object) {
		// Check for null reference and matching types. 
		if ((object != null) && (this.getClass().equals(object.getClass()))) {
			final Fraction other = (Fraction) object;
			return this.numerator == other.numerator && this.denominator == other.denominator;
		}
		else {
			return false;
		}
	} // end method equals
	
	
	/**
	 * Returns a hash code for the <code>Fraction</code>. 
	 * 
	 * @return	the hash code for the Fraction 
	 */ 
	@Override
	public int hashCode() {
		return Objects.hash(this.numerator,this.denominator);
	}

	public static int gcd(int p, int q) {
		while (q != 0) {
			final int temp = q;
			q = p % q;
			p = temp;
		}
		return p < 0 ? -p : p;
	}


} // end class Fraction