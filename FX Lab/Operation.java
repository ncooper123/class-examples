import java.util.HashMap;
import java.util.function.BiFunction;

public enum Operation {

    ADD("+",Fraction::add),
    SUBTRACT("-",Fraction::subtract),
    MULTIPLY("*",Fraction::multiply),
    DIVIDE("/",Fraction::divide);

    public final String symbol;
    private final BiFunction<Fraction,Fraction,Fraction> f;

    Operation (final String symbol, final BiFunction<Fraction,Fraction,Fraction> f){
        this.symbol = symbol;
        this.f = f;
    }


    public Fraction operate(final Fraction a, final Fraction b){
        return this.f.apply(a,b);
    }

    @Override
    public String toString(){
        return this.symbol;
    }

}
