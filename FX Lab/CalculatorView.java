import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.util.Observable;
import java.util.Observer;


public class CalculatorView extends Application implements Observer {

    private Calculator calculator;
    private FractionBox left, right, result;
    private ComboBox<Operation> operationComboBox;
    private Button calculateButton;

    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(final Stage stage){
        stage.setTitle("Fraction Calculator");
        this.calculator = new Calculator(Fraction.ZERO,Operation.ADD,Fraction.ZERO);
        this.calculator.addObserver(this);
        this.setupNodes(stage);
        this.setupListeners();
        this.update(this.calculator,null);
        stage.show();
    }

    private void showError(final String text){
        final Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("There was an error performing your computation:");
        alert.setContentText(text);
        alert.showAndWait();
    }

    private void setupNodes(final Stage stage){
        final VBox parent = new VBox();
        parent.setAlignment(Pos.CENTER);
        stage.setScene(new Scene(parent,400,150));

        // Setup the problem.
        final HBox problem = new HBox();
        problem.setAlignment(Pos.CENTER);

        // Left fraction.
        this.left = new FractionBox(true);
        problem.getChildren().add(this.left);

        // The operation.
        this.operationComboBox = new ComboBox<Operation>();
        this.operationComboBox.getItems().addAll(Operation.values());
        this.operationComboBox.getSelectionModel().select(0);
        problem.getChildren().add(this.operationComboBox);

        // Right fraction.
        this.right = new FractionBox(true);
        problem.getChildren().add(this.right);

        // Equals label.
        Label equals = new Label("=");
        problem.getChildren().add(equals);

        // Result fraction.
        this.result = new FractionBox(false);
        problem.getChildren().add(this.result);

        parent.getChildren().add(problem);

        // Setup the calculate button.
        this.calculateButton = new Button("Calculate");
        parent.getChildren().add(this.calculateButton);
    }

    private void setupListeners() {
        this.calculateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    final Operation newOperation = CalculatorView.this.operationComboBox.getValue();
                    final Fraction newLeft = CalculatorView.this.left.getFraction();
                    final Fraction newRight = CalculatorView.this.right.getFraction();
                    CalculatorView.this.calculator.update(newLeft, newOperation, newRight);
                } catch (Exception e){
                    CalculatorView.this.showError(e.getMessage());
                }
            }
        });
    }

    // Respond to an update in the model by refreshing our view.
    @Override
    public void update(final Observable o, final Object argument){
        if (o == this.calculator){
            this.left.updateWithFraction(this.calculator.getA());
            this.right.updateWithFraction(this.calculator.getB());
            this.operationComboBox.getSelectionModel().select(this.calculator.getOperation());
            this.result.updateWithFraction(this.calculator.getResult());
        }
    }

    private static class FractionBox extends VBox {

        private final TextField numerator;
        private final TextField denominator;

        FractionBox (final boolean editable){
            this.setAlignment(Pos.CENTER);
            this.setPadding(new Insets(10,10,10,10));
            this.numerator = new TextField("");
            configureTextField(this.numerator,editable);
            this.getChildren().add(this.numerator);
            final Label bar = new Label("──────");
            this.getChildren().add(bar);
            this.denominator = new TextField("");
            configureTextField(this.denominator,editable);
            this.getChildren().add(this.denominator);
        }

        // Returns the fraction represented by this FractionBox.
        public Fraction getFraction() throws NumberFormatException, ArithmeticException {
            final int p = Integer.parseInt(this.numerator.getText());
            final int q = Integer.parseInt(this.denominator.getText());
            if (q == 0){
                throw new ArithmeticException("Division by Zero.");
            }
            return new Fraction(p,q,false);
        }

        // Updates this FractionBox to hold a new fraction.
        public void updateWithFraction(final Fraction fraction){
            this.numerator.setText(String.valueOf(fraction.getNumerator()));
            this.denominator.setText(String.valueOf(fraction.getDenominator()));
        }

        // Helper to configure each box.
        private static void configureTextField(TextField textField, boolean editable){
            textField.setPrefWidth(30);
            textField.setEditable(editable);
            textField.setAlignment(Pos.CENTER);
        }

    }

}
