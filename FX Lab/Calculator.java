import java.util.Observable;

public class Calculator extends Observable {

    private Fraction a;
    private Operation operation;
    private Fraction b;
    private Fraction result;

    public Calculator(final Fraction a, final Operation operation, final Fraction b){
        this.a = a;
        this.b = b;
        this.operation = operation;
        this.updateResult();
    }

    public Fraction getA() {
        return a;
    }

    public Operation getOperation() {
        return operation;
    }

    public Fraction getB() {
        return b;
    }

    public Fraction getResult(){
        return this.result;
    }

    public void update(final Fraction a, final Operation operation, final Fraction b){
        this.a = a;
        this.operation = operation;
        this.b = b;
        this.updateResult();
    }

    private void updateResult(){
        this.result = this.operation.operate(this.a,this.b);
        this.setChanged();
        this.notifyObservers();
    }

}