
public class Maximum {

    public static <T extends Comparable<T>> T max(T... values){
        if (values.length == 0){
            throw new IllegalArgumentException("No values to compare.");
        }
        T max = values[0];
        for (int i = 1; i < values.length; i++){
            if (values[i].compareTo(max) > 0){
                max = values[i];
            }
        }
        return max;
    }


    public static void main(String[] args){
        System.out.println(max(3,3,3));
        System.out.println(max(3,3,3,6));
        System.out.println(max(3,3,3,6,10,3));
    }

}
