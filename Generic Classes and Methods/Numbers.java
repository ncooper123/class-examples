import java.util.List;
import java.util.ArrayList;

public class Numbers {

    /**
     * Returns the sum of a list of numbers.
     *
     * This method uses the WILDCARD OPERATOR, "?". It states that the
     * type parameter of the List argument must BE-A Number.
     *
     * If we had just said List<Number> instead, the method would not be able
     * to accept, say, a List<Integer>, since List<Integer> IS NOT A List<Number>
     * (even though Integer is a subclass of Number).
     *
     * Alternatively, we could have written the method declaration like this:
     *
     * public static <T extends Number> double sum(List<T> list) ...
     *
     * to achieve the same result.
     *
     * @param list The list to get the total of.
     * @return The total of the list.
     */
    public static double sum(List<? extends Number> list){
        double total = 0;
        for (Number number : list){
            // We can call .doubleValue() here since we know elements in the list ARE numbers.
            total += number.doubleValue();
        }
        return total;
    }

    public static void main(String[] args){
        final Number[] numbers = {1, 3, 6, 7};
        final ArrayList<Number> numberList = new ArrayList<>();
        for (Number element : numbers){
            numberList.add(element);
        }

        double total = sum(numberList);
        System.out.println("The total is: " + total);
    }

}
