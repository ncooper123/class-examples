public class OverloadedMethods {

    /**
     * Prints the getArrea() value of all the items in an array.
     *
     * This is a GENERIC method. It declares a type variable E that must be a subclass of Shape. Therefore
     * any Shape[] or array of a subtype of Shape is permitted as a parameter.
     *
     * @param array The array to print.
     */
    public static <E extends Shape> void printArray(E[] array){
        for (E element : array){
            // We know E is some subtype of shape.
            System.out.println(element.getArea());
        }
    }

    public static void main(String[] args){
        Shape[] myShapes = new Shape[]{new Rectangle(1,3),new Square(3), new Circle(4)};
        printArray(myShapes);
    }

}